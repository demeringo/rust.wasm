FROM gitpod/workspace-full

USER gitpod

RUN brew install httpie && \
    brew install hey && \
    curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh 

